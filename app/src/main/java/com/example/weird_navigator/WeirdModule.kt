package com.example.weird_navigator

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val weirdModule = module {
    viewModel { WeirdViewModel() }
}