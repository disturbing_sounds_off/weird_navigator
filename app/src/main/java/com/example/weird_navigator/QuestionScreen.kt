package com.example.weird_navigator

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Card
import androidx.compose.material3.CardColors
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun QuestionScreen(
    modifier: Modifier = Modifier,
    question: Question,
    updateQuestionBody: (Int, String) -> Unit,
    updateQuestionScore: (Int, Int) -> Unit
) {
    Column(
        modifier.background(Color(0xFF4E3A94)),
//        modifier.background(Color.White),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        Card(modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
            colors = CardDefaults.cardColors(
                containerColor = Color.White
            ),
            shape = RoundedCornerShape(
                topStart = 8.dp,
                topEnd = 40.dp,
                bottomStart = 40.dp,
                bottomEnd = 8.dp
            )
        ) {

            OutlinedTextField(
                modifier = Modifier
                    .padding(horizontal = 24.dp, vertical = 16.dp)
                    .fillMaxWidth(),
                value = question.body,
                onValueChange = { newBody -> updateQuestionBody(question.id, newBody) },
                label = { Text(text = "Question")},
                textStyle = MaterialTheme.typography.titleLarge.copy(
//                    fontWeight = FontWeight.SemiBold,
                    color = Color(0xFF4E3A94)
                ),
                colors = OutlinedTextFieldDefaults.colors(
                    focusedContainerColor = primaryColor,
                    unfocusedBorderColor = primaryColor,
                    unfocusedLabelColor = primaryColor

                ),
//                shape = RoundedCornerShape(18.dp)
            )


        WeirdTextField(
            modifier = Modifier
                .padding(horizontal = 24.dp, vertical = 16.dp)
                .fillMaxWidth(),
            value = question.body,
            onValueChange = { newBody -> updateQuestionBody(question.id, newBody) },
            textStyle = MaterialTheme.typography.titleLarge.copy(
//                fontWeight = FontWeight.SemiBold,
                color = Color(0xFF4E3A94)
            )
        )

        }


        Spacer(modifier = Modifier.size(32.dp))
        Box(modifier = Modifier
            .size(42.dp)
            .clip(RoundedCornerShape(8.dp))
            .background(color = Color.White),
            contentAlignment = Alignment.Center,

        ) {
            BasicTextField(
                value = question.score.toString(),
                onValueChange = {newScore ->
                    if (newScore.length < 3){
                        updateQuestionScore(question.id, newScore.toIntOrNull() ?: 0)
                    }
                },
                textStyle = MaterialTheme.typography.titleLarge.copy(
                    color = Color(0xFF4E3A94),
                    fontWeight = FontWeight.SemiBold,
                    textAlign = TextAlign.Center
                ),

            )
        }
    }

}

@Preview
@Composable
fun QuestionScreenPrev() {
    Surface {
        QuestionScreen(
            modifier = Modifier.fillMaxSize(),
            question = Question(
                id = 0,
                body = "How many fucks do you think i give about contents of this question?",
                score = 2
            ),
            {a,b -> },
            {a,b -> }
        )
    }
}