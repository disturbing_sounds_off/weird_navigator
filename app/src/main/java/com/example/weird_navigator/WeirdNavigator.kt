package com.example.weird_navigator

import android.util.Log
import androidx.compose.animation.slideInHorizontally
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.rounded.DateRange
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import org.koin.androidx.compose.koinViewModel

@Composable
fun WeirdNavigator() {

    val navController: NavHostController = rememberNavController()
//    val vm = viewModel<WeirdViewModel>()
    val vm: WeirdViewModel = koinViewModel()
    val state by vm.questionState.collectAsState()

    Scaffold(
        floatingActionButton = {
            IconButton(
                modifier = Modifier
                    .clip(RoundedCornerShape(16.dp))
                    .background(Color.White),
                onClick = vm::addQuestion
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    tint = Color(0xFF4E3A94),
                    contentDescription = ""
                )
            }
        },
        bottomBar = {
            NavBottomBar(
                questions = state.questions,
                currentQuestionId = state.currentQuestionId,
                addQuestion = vm::addQuestion,
                navigateTo = {index ->
                    navController.popBackStack()
                    vm.updateCurrentQuestionId(index)
                    navController.navigate("question/$index")
                }
            )
        }
    ) {
        Column(Modifier.padding(it)) {
            NavHostHolder(
                modifier = Modifier.weight(1f),
                navController = navController,
                vm = vm,
                state = state
            )
        }
    }

}

@Composable
fun NavHostHolder(
    modifier: Modifier,
    navController: NavHostController,
    vm: WeirdViewModel,
    state: WeirdState
) {
    NavHost(modifier = modifier,navController = navController, startDestination = "question/1"){

        composable(
            route = "question/{questionId}",
            arguments = listOf(
                navArgument("questionId") {type = NavType.IntType}
            ),
            enterTransition = {
                if (state.currentQuestionId > state.prevQuestionId){
                    slideInHorizontally(initialOffsetX = {it})
                }
                else {
                    slideInHorizontally(initialOffsetX = {-it})
                }
            }
        ) {backStack ->
            backStack.arguments?.getInt("questionId")
                ?.let {
                    QuestionScreen(
                        modifier = modifier,
                        question = state.questions[it],
                        updateQuestionBody = vm::updateQuestionBody,
                        updateQuestionScore = vm::updateQuestionScore
                    )
                }
        }
    }
}

@Composable
fun NavBottomBar(
    modifier: Modifier = Modifier,
    questions: List<Question>,
    currentQuestionId: Int,
    addQuestion: () -> Unit,
    navigateTo: (Int) -> Unit
) {
    Row (
        modifier
            .fillMaxWidth()
            .background(Color.White)
    ){

        LazyRow {
            item {
                NavBottomIcon (
                    modifier = Modifier.clickable { Log.d("FINDME", "NavBottomBar: clicked calendar") },
                ){
                    Icon(imageVector = Icons.Rounded.DateRange, contentDescription = "")
                }

            }
            items(items = questions, key = {item -> item.id }) {question ->
                NavBottomIcon(
                    modifier = Modifier.clickable { navigateTo(question.id) },
                    selected = question.id == currentQuestionId,
                ){
                    Text(
                        modifier = Modifier.padding(horizontal = 6.dp),
                        text = (question.id + 1).toString(),
                        style = TextStyle(
                            color = if (question.id == currentQuestionId) Color(0xFF4E3A94)
                            else Color.White
                        )
                    )
                }

            }
        }

    }
}

@Composable
fun NavBottomIcon(
    modifier: Modifier = Modifier,
    selected: Boolean = false,
    content: @Composable () -> Unit
) {
    Box(
        modifier = modifier
            .height(66.dp)
            .aspectRatio(1f)
            .padding(vertical = 6.dp, horizontal = 8.dp)
            .border(
                width = 1.dp,
                color = if (selected) Color(0xFF4E3A94)
                else Color.White,
                shape = RoundedCornerShape(16.dp)
            )
            .clip(RoundedCornerShape(16.dp))
            .background(
                if (selected) Color.White
                else Color(0xFF4E3A94)
            ),
        contentAlignment = Alignment.Center
    ) {
        content()
    }
}