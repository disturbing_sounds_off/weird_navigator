package com.example.weird_navigator

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material.icons.rounded.Delete
import androidx.compose.material.icons.sharp.Delete
import androidx.compose.material.icons.twotone.Delete
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.RadioButtonDefaults
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

val textFieldColor = Color(0xFFE0DDEC)
val primaryColor = Color(0xFF4D328F)
val secondaryColor = Color(0xFFB747DF)
val bacisTextStyle = TextStyle(
    fontSize = 14.sp,
    color = Color(0xFF4E3A94)
)

@Composable
fun WeirdTextField(
    modifier: Modifier = Modifier,
    value: String,
    onValueChange: (String) -> Unit,
    textStyle: TextStyle = bacisTextStyle
) {
    BasicTextField(
        modifier = modifier
            .heightIn(min = 44.dp)
        ,
        value = value,
        onValueChange = onValueChange,
        textStyle = textStyle,
        decorationBox = {innerTextField ->
            Row (
                Modifier
                    .clip(RoundedCornerShape(6.dp))
                    .background(color = textFieldColor)
                    .padding(horizontal = 8.dp, vertical = 12.dp)
                    ,
                verticalAlignment = Alignment.CenterVertically

            ){
                innerTextField()
            }
        }
    )
}

@Composable
fun WeirdOutlinedTextField(
    modifier: Modifier = Modifier,
    value: String,
    onValueChange: (String) -> Unit,
    textStyle: TextStyle = bacisTextStyle
) {
    BasicTextField(
        modifier = modifier
            .heightIn(min = 44.dp)
        ,
        value = value,
        onValueChange = onValueChange,
        textStyle = textStyle,
        decorationBox = {innerTextField ->
            Row (
                Modifier
                    .border(
                        width = 2.dp,
                        color = primaryColor,
                        shape = RoundedCornerShape(size = 6.dp)
                    )
                    .clip(RoundedCornerShape(6.dp))
                    .background(color = textFieldColor)
                    .padding(horizontal = 8.dp, vertical = 12.dp)
                ,
                verticalAlignment = Alignment.CenterVertically

            ){
                innerTextField()
            }
        }
    )
}

@Preview(showSystemUi = true)
@Composable
fun WeirdTextFieldPrev() {
    Surface {
        Column(modifier = Modifier
            .fillMaxSize()
            .background(Color.White),
            verticalArrangement = Arrangement.Center
        ){
            ExampleRowForWeirdTextField("Текст первого ответа")
            Spacer(Modifier.size(8.dp))
            ExampleRowForWeirdTextField("Оооочень длинный текст как пример ответа, который будет состоять из огромного набора символов и скорее всего сломает наш дизайн")
            Spacer(Modifier.size(8.dp))
            ExampleRowForWeirdTextField("еще один пример вопроса, чтобы здорово было посмотреть")
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun WeirdOutlinedTextFieldPrev() {
    Surface {
        Column(modifier = Modifier
            .fillMaxSize()
            .background(Color.White),
            verticalArrangement = Arrangement.Center
        ){
            ExampleRowForWeirdOutlinedTextField("Текст первого ответа")
            Spacer(Modifier.size(8.dp))
            ExampleRowForWeirdOutlinedTextField("Оооочень длинный текст как пример ответа, который будет состоять из огромного набора символов и скорее всего сломает наш дизайн")
            Spacer(Modifier.size(8.dp))
            ExampleRowForWeirdOutlinedTextField("еще один пример вопроса, чтобы здорово было посмотреть")
        }
    }
}

@Composable
fun ExampleRowForWeirdTextField(exampleText: String) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
//            .padding(32.dp),
//                horizontalArrangement = Arrangement.SpaceBetween,
                ,
        verticalAlignment = Alignment.Top
    ){
        RadioButton(
            modifier = Modifier.size(44.dp),
            selected = true,
            onClick = { /*TODO*/ },
            colors = RadioButtonDefaults.colors(
                selectedColor = secondaryColor,
                unselectedColor = secondaryColor
            )
        )
        WeirdTextField(
            modifier = Modifier
                .weight(1f)
                .padding(end = 16.dp),
//            value = "Текст для первого вопроса первого теста для макета приложения",
            value = exampleText,
            onValueChange = {}
        )

        Box(
            modifier = Modifier
                .padding(end = 16.dp)
                .clip(RoundedCornerShape(16.dp))
                .size(44.dp)
                .background(secondaryColor)
            ,
            contentAlignment = Alignment.Center
        ) {
            Icon(
                painter = painterResource(id = R.drawable.cool_delete),
                contentDescription = "",
                tint = Color.White,

            )
        }
    }

}
@Composable
fun ExampleRowForWeirdOutlinedTextField(exampleText: String) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
//            .padding(32.dp),
//                horizontalArrangement = Arrangement.SpaceBetween,
        ,
        verticalAlignment = Alignment.Top
    ){
        RadioButton(
            modifier = Modifier.size(44.dp),
            selected = true,
            onClick = { /*TODO*/ },
            colors = RadioButtonDefaults.colors(
                selectedColor = secondaryColor,
                unselectedColor = secondaryColor
            )
        )
        WeirdOutlinedTextField(
            modifier = Modifier
                .weight(1f)
                .padding(end = 16.dp),
//            value = "Текст для первого вопроса первого теста для макета приложения",
            value = exampleText,
            onValueChange = {}
        )

        Box(
            modifier = Modifier
                .padding(end = 16.dp)
                .clip(RoundedCornerShape(16.dp))
                .size(44.dp)
                .background(secondaryColor)
            ,
            contentAlignment = Alignment.Center
        ) {
            Icon(
                painter = painterResource(id = R.drawable.cool_delete),
                contentDescription = "",
                tint = Color.White,

                )
        }
    }

}
