package com.example.weird_navigator

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class WeirdViewModel : ViewModel(){

    private val _questionState = MutableStateFlow(WeirdState())
    val questionState = _questionState.asStateFlow()

    fun addQuestion(): Int {
        val newQuestion = Question(
            id = _questionState.value.questions.last().id + 1,
            body = "",
            score = 0,
        )

        _questionState.update { it.copy(
            questions = it.questions + newQuestion
        ) }
        return -1
    }

    private fun addQuestion(question: Question) {
        _questionState.update { it.copy(
            questions = it.questions + question
        ) }
    }

    fun updateQuestionBody(id: Int, newBody: String){
        val updatedQuestions = _questionState.value.questions.toMutableList()
        updatedQuestions[id] = updatedQuestions[id].copy(body = newBody)
        _questionState.update { it.copy( questions = updatedQuestions) }
    }

    fun updateQuestionScore(id: Int, newScore: Int){
        val updatedQuestions = _questionState.value.questions.toMutableList()
        updatedQuestions[id] = updatedQuestions[id].copy(score = newScore)
        _questionState.update { it.copy( questions = updatedQuestions) }
    }

    fun updateCurrentQuestionId(newId: Int) {
        _questionState.update {it.copy(
            prevQuestionId =  it.currentQuestionId,
            currentQuestionId = newId,
        )}
    }

    init {
        addQuestion(Question(id = 0, body = "How old am i ?", score = 5 ))
        addQuestion(Question(id = 1, body = "How many legs do i have ?", score = 2))
        addQuestion(Question(id = 2, body = "Is it true that gradle sucks bbl (big blue lollipop) ?", score = 10))
    }
}


data class WeirdState(
    val questions: List<Question> = emptyList(),
    val currentQuestionId: Int = 0,
    val prevQuestionId: Int = 0,
)
data class Question(
    val id: Int,
    var body: String,
    var score: Int,
)
